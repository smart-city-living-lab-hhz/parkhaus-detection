from pipeline import (PipelineRunner, ContourDetection,
                      Visualizer, CsvWriter, VehicleCounter)
import os
import logging
import logging.handlers
import random
import numpy as np
import skvideo.io
import cv2
import matplotlib.pyplot as plt
import utils
import time
# without this some strange errors happen
cv2.ocl.setUseOpenCL(False)
random.seed(123)

IMAGE_DIR = "./out"
SHAPE = (324, 576)
P1 = round(SHAPE[0]/3)
P2 = P1*2
EXIT_PTS = np.array([
    [[0, SHAPE[0]], [0, P2], [SHAPE[1], P2], [SHAPE[1], SHAPE[0]]],
    [[0, P1], [SHAPE[1], P1], [SHAPE[1], 0], [0, 0]]])
# ============================================================================

def main():
    log = logging.getLogger("main")

    # creating exit mask from points, where we will be counting our vehicles
    base = np.zeros(SHAPE + (3,), dtype='uint8')
    exit_mask = cv2.fillPoly(base, EXIT_PTS, (255, 255, 255))[:, :, 0]

    # there is also bgslibrary, that seems to give better BG substruction, but not tested it yet
    bg_subtractor = cv2.createBackgroundSubtractorMOG2(
        history=500, detectShadows=True)

    # processing pipline for programming conviniance
    pipeline = PipelineRunner(pipeline=[ContourDetection(bg_subtractor=bg_subtractor, save_image=True, image_dir=IMAGE_DIR),
                                        # we use y_weight == 2.0 because traffic are moving vertically on video
                                        # use x_weight == 2.0 for horizontal.
                                        VehicleCounter(exit_masks=[exit_mask], y_weight=2.0),
                                        # testbilder mit Visualizer:
                                        Visualizer(image_dir=IMAGE_DIR)
										], log_level=logging.INFO)

    cap = cv2.VideoCapture(0)
    # Setting Resolution - only worked on my pi as my webcam can't provide the resolution. In case of cv2 -215 Assertion error, the frame has likely the wrong resolution
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, SHAPE[1])
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, SHAPE[0])
    if not cap.isOpened(): 
      # Sometimes cap is not opened right away so we need to do this manually
      cap.open()

    frame_number = -1
    t0 = time.time()
    while round(time.time() - t0, 2) < 86400: # loop runs for 60s*60*24 = 24hours
      ret,frame = cap.read()
      if not ret:
        continue
      
      frame_number += 1
      pipeline.set_context({'frame': frame, 'frame_number': frame_number, })
      pipeline.run()

    cap.release()

# ============================================================================
if __name__ == "__main__":
    log = utils.init_logging()

    if not os.path.exists(IMAGE_DIR):
        log.debug("Creating image directory `%s`...", IMAGE_DIR)
        os.makedirs(IMAGE_DIR)

    main()
